from django.shortcuts import render


def home(request):
    return render(request, 'tasker/home.html')
